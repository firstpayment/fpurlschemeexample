package kr.co.firstpayment.app.target;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Toast;

import java.net.URLEncoder;

public class MainActivity extends AppCompatActivity {

    private DataForPay data;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
    @Override
    protected void onResume() {
        super.onResume();

        Intent intent = getIntent();
        if (Intent.ACTION_VIEW.equals(intent.getAction())) {
            Uri uri = intent.getData();

            try {
                data = new DataForPay(uri);
            }catch(Exception e) {
                Toast.makeText(this, e.toString(), Toast.LENGTH_SHORT).show();
            }
        }
    }
    public void 카드결제성공콜백(View view){
        try {
            String cardCashSe = "CARD";
            String delngSe = "1";
            String setleSuccesAt = "O";
            String setleMessage=URLEncoder.encode("잔액 : 0000", "UTF-8");
            String confmNo = "43456853";
            String confmDe = "160215";
            String confmTime = "140701";
            String cardNo = "1234-23XX-XXXX-XX23";
            String instlmtMonth = "0";
            String issuCmpnyCode = "12";
            String issuCmpnyNm = URLEncoder.encode("현대카드사", "UTF-8");
            String puchasCmpnyCode = "12";
            String puchasCmpnyNm = URLEncoder.encode("현대카드사", "UTF-8");
            String aditInfo = URLEncoder.encode(data.getAditInfo(), "UTF-8");

            String url = data.getCallbackAppUrl()+"?"+
                         "cardCashSe="+cardCashSe+
                         "&delngSe="+delngSe+
                         "&setleSuccesAt="+setleSuccesAt+
                         "&setleMssage="+setleMessage+
                         "&confmNo="+confmNo+
                         "&confmDe="+confmDe+
                         "&confmTime="+confmTime+
                         "&cardNo="+cardNo+
                         "&instlmtMonth="+instlmtMonth+
                         "&issuCmpnyCode="+issuCmpnyCode+
                         "&issuCmpnyNm="+issuCmpnyNm+
                         "&puchasCmpnyCode="+puchasCmpnyCode+
                         "&puchasCmpnyNm="+puchasCmpnyNm+
                         "&aditInfo="+aditInfo;

            콜백호출(url);

        }catch(Exception e){
            e.printStackTrace();
        }
    }
    public void 카드결제실패콜백(View view){
        try {
            String cardCashSe = "CARD";
            String delngSe = "0";
            String setleSuccesAt = "X";
            String setleMessage=URLEncoder.encode("유효하지 않은 카드번호", "UTF-8");
            String confmNo = "";
            String confmDe = "";
            String confmTime = "";
            String cardNo = "";
            String instlmtMonth = "";
            String issuCmpnyCode = "";
            String issuCmpnyNm = "";
            String puchasCmpnyCode = "";
            String puchasCmpnyNm = "";
            String aditInfo = URLEncoder.encode(data.getAditInfo(), "UTF-8");

            String url = data.getCallbackAppUrl()+"?"+
                    "cardCashSe="+cardCashSe+
                    "&delngSe="+delngSe+
                    "&setleSuccesAt="+setleSuccesAt+
                    "&setleMssage="+setleMessage+
                    "&confmNo="+confmNo+
                    "&confmDe="+confmDe+
                    "&confmTime="+confmTime+
                    "&cardNo="+cardNo+
                    "&instlmtMonth="+instlmtMonth+
                    "&issuCmpnyCode="+issuCmpnyCode+
                    "&issuCmpnyNm="+issuCmpnyNm+
                    "&puchasCmpnyCode="+puchasCmpnyCode+
                    "&puchasCmpnyNm="+puchasCmpnyNm+
                    "&aditInfo="+aditInfo;

            콜백호출(url);

        }catch(Exception e){
            e.printStackTrace();
        }
    }
    public void 현금결제성공콜백(View view){
        try {
            String cardCashSe = "CASH";
            String delngSe = "1";
            String setleSuccesAt = "O";
            String setleMessage=URLEncoder.encode("현금영수증 문의는 국번없이 124", "UTF-8");
            String confmNo = "43456853";
            String confmDe = "160215";
            String confmTime = "140701";
            String cardNo = "0100001234";
            String instlmtMonth = "0";
            String issuCmpnyCode = "";
            String issuCmpnyNm = "";
            String puchasCmpnyCode = "";
            String puchasCmpnyNm = "";
            String aditInfo = URLEncoder.encode(data.getAditInfo(), "UTF-8");

            String url = data.getCallbackAppUrl()+"?"+
                    "cardCashSe="+cardCashSe+
                    "&delngSe="+delngSe+
                    "&setleSuccesAt="+setleSuccesAt+
                    "&setleMssage="+setleMessage+
                    "&confmNo="+confmNo+
                    "&confmDe="+confmDe+
                    "&confmTime="+confmTime+
                    "&cardNo="+cardNo+
                    "&instlmtMonth="+instlmtMonth+
                    "&issuCmpnyCode="+issuCmpnyCode+
                    "&issuCmpnyNm="+issuCmpnyNm+
                    "&puchasCmpnyCode="+puchasCmpnyCode+
                    "&puchasCmpnyNm="+puchasCmpnyNm+
                    "&aditInfo="+aditInfo;

            콜백호출(url);

        }catch(Exception e){
            e.printStackTrace();
        }
    }
    public void 현금결제실패콜백(View view){
        try {
            String cardCashSe = "CASH";
            String delngSe = "0";
            String setleSuccesAt = "X";
            String setleMessage=URLEncoder.encode("이미 취소된 거래입니다.", "UTF-8");
            String confmNo = "";
            String confmDe = "";
            String confmTime = "";
            String cardNo = "";
            String instlmtMonth = "";
            String issuCmpnyCode = "";
            String issuCmpnyNm = "";
            String puchasCmpnyCode = "";
            String puchasCmpnyNm = "";
            String aditInfo = URLEncoder.encode(data.getAditInfo(), "UTF-8");

            String url = data.getCallbackAppUrl()+"?"+
                    "cardCashSe="+cardCashSe+
                    "&delngSe="+delngSe+
                    "&setleSuccesAt="+setleSuccesAt+
                    "&setleMssage="+setleMessage+
                    "&confmNo="+confmNo+
                    "&confmDe="+confmDe+
                    "&confmTime="+confmTime+
                    "&cardNo="+cardNo+
                    "&instlmtMonth="+instlmtMonth+
                    "&issuCmpnyCode="+issuCmpnyCode+
                    "&issuCmpnyNm="+issuCmpnyNm+
                    "&puchasCmpnyCode="+puchasCmpnyCode+
                    "&puchasCmpnyNm="+puchasCmpnyNm+
                    "&aditInfo="+aditInfo;

            콜백호출(url);

        }catch(Exception e){
            e.printStackTrace();
        }
    }
    private void 콜백호출(String urlData){
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(urlData));
        startActivity(intent);
    }
}

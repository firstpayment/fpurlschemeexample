package kr.co.firstpayment.app.target;

import android.net.Uri;

import java.text.ParseException;
import java.text.SimpleDateFormat;

public class DataForPay{

    private String bizrno;
    private String trmnlNo;
    private String cardCashSe;
    private String delngSe;
    private String splpc;
    private String vat;
    private String taxxpt;
    private String callbackAppUrl;
    private String aditInfo;
    private String srcConfmNo;
    private String srcConfmDe;
    private String srcInstlmtMonth;


    public DataForPay(Uri uri) throws Exception{

        bizrno = uri.getQueryParameter("bizrno");
        trmnlNo = uri.getQueryParameter("trmnlNo");
        cardCashSe = uri.getQueryParameter("cardCashSe");
        delngSe = uri.getQueryParameter("delngSe");
        splpc = uri.getQueryParameter("splpc");
        vat = uri.getQueryParameter("vat");
        taxxpt = uri.getQueryParameter("taxxpt");
        callbackAppUrl = uri.getQueryParameter("callbackAppUrl");
        aditInfo = uri.getQueryParameter("aditInfo");
        srcConfmNo = uri.getQueryParameter("srcConfmNo");
        srcConfmDe = uri.getQueryParameter("srcConfmDe");
        srcInstlmtMonth = uri.getQueryParameter("srcInstlmtMonth");

        if(bizrno.equals("")){
            throw new Exception("bizrno is empty");
        }else{
            //하이픈은 무조건 제거한다.
            bizrno= bizrno.replaceAll("-", "");
            try{
                Integer.parseInt(bizrno);
            }catch(Exception e){
                throw new Exception("bizrno is invalid");
            }
        }

        if(trmnlNo.equals("")){
            throw new Exception("trmnlNo is empty");
        }

        if(cardCashSe.equals("")){
            throw new Exception("cardCashSe is empty");
        }else{
            if(!(cardCashSe.equals("CARD")|| cardCashSe.equals("CASH"))){
                throw new Exception("cardCashSe is invalid (only use CARD or CASH))");
            }
        }

        if(delngSe.equals("")){
            throw new Exception("delngSe is empty");
        }else{
            if(!(delngSe.equals("0")|| delngSe.equals("1"))){
                throw new Exception("delngSe is invalid (only use 0 or 1))");
            }
        }

        if(splpc.equals("")){
            throw new Exception("splpc is empty");
        }else{
            try{
                Double.parseDouble(splpc);
            }catch(Exception e){
                throw new Exception("splpc is not number");
            }
        }

        if(vat.equals("")){
            throw new Exception("vat is empty");
        }else{
            try{
                Double.parseDouble(vat);
            }catch(Exception e){
                throw new Exception("vat is not number");
            }
        }

        if(taxxpt.equals("")){
            throw new Exception("taxxpt is empty");
        }else{
            try{
                Double.parseDouble(taxxpt);
            }catch(Exception e){
                throw new Exception("taxxpt is not number");
            }
        }

        if(callbackAppUrl.equals("")){
            throw new Exception("callbackAppUrl is empty");
        }

        if(delngSe.equals("0")) { //승인취소처리의경우..

            if (srcConfmNo.equals("")) {
                throw new Exception("srcConfmNo is empty");
            }

            if (srcConfmDe.equals("")) {
                throw new Exception("srcConfmDe is empty");
            }else{
                try {
                    SimpleDateFormat sdf = new SimpleDateFormat("yyMMdd");
                    sdf.parse(srcConfmDe);
                }catch(ParseException e){
                    throw new Exception("srcConfmDe is not specified date format(yyMMdd)");
                }
            }

            if (srcInstlmtMonth.equals("")) {
                throw new Exception("srcInstlmtMonth is empty");
            }else{

                try{
                    Integer.parseInt(srcInstlmtMonth);
                }catch(Exception e){
                    throw new Exception("srcInstlmtMonth is invalid (only use number)");
                }

                if(cardCashSe.equals("CASH")) {
                    if(!(srcInstlmtMonth.equals("0")|| srcInstlmtMonth.equals("1"))){
                        throw new Exception("srcInstlmtMonth is invalid (only use 0 or 1 under CASH))");
                    }
                }else{
                    //no need..
                }
            }
        }
    }

    public String getBizrno() {
        return bizrno;
    }

    public String getTrmnlNo() {
        return trmnlNo;
    }

    public String getCardCashSe() {
        return cardCashSe;
    }

    public String getDelngSe() {
        return delngSe;
    }

    public String getSplpc() {
        return splpc;
    }

    public String getVat() {
        return vat;
    }

    public String getTaxxpt() {
        return taxxpt;
    }

    public String getCallbackAppUrl() {
        return callbackAppUrl;
    }

    public String getAditInfo() {
        return aditInfo==null?"":aditInfo;
    }

    public String getSrcConfmNo() {
        return srcConfmNo;
    }

    public String getSrcConfmDe() {
        return srcConfmDe;
    }

    public String getSrcInstlmtMonth() {
        return srcInstlmtMonth;
    }
}
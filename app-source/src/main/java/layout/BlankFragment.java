package layout;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.net.URLDecoder;

import kr.co.firstpayment.app.urlscheme.R;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link BlankFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link BlankFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class BlankFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public BlankFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment BlankFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static BlankFragment newInstance(String param1, String param2) {
        BlankFragment fragment = new BlankFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_blank, container, false);
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Activity context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    public void onNewIntent(Intent intent) {
        //URLScheme방식으로 호출된경우 결과정보를 얻어서 처리..
        if (Intent.ACTION_VIEW.equals(intent.getAction())) {
            Uri uri = intent.getData();

            try {

                String cardCashSe = uri.getQueryParameter("cardCashSe");
                String delngSe = uri.getQueryParameter("delngSe");
                String setleSuccesAt = uri.getQueryParameter("setleSuccesAt");
                String setleMssage = URLDecoder.decode(uri.getQueryParameter("setleMssage"), "UTF-8");

                String confmNo;
                String confmDe;
                String confmTime;
                String cardNo;
                String instlmtMonth;
                String issuCmpnyCode;
                String issuCmpnyNm;
                String puchasCmpnyCode;
                String puchasCmpnyNm;

                String aditInfo;

                String 출력정보 = "";

                if (cardCashSe.equals("CARD")) {
                    if (setleSuccesAt.equals("O")) {
                        confmNo = uri.getQueryParameter("confmNo");
                        confmDe = uri.getQueryParameter("confmDe");
                        confmTime = uri.getQueryParameter("confmTime");
                        cardNo = uri.getQueryParameter("cardNo");
                        instlmtMonth = uri.getQueryParameter("instlmtMonth");
                        issuCmpnyCode = uri.getQueryParameter("issuCmpnyCode");
                        issuCmpnyNm = URLDecoder.decode(uri.getQueryParameter("issuCmpnyNm"), "UTF-8");
                        puchasCmpnyCode = uri.getQueryParameter("puchasCmpnyCode");
                        puchasCmpnyNm = URLDecoder.decode(uri.getQueryParameter("puchasCmpnyNm"), "UTF-8");
                        aditInfo = URLDecoder.decode(uri.getQueryParameter("aditInfo"), "UTF-8");

                        출력정보 = "결제처리결과 : " + setleSuccesAt + "\n" +
                                "결제메세지 : " + setleMssage + "\n" +
                                "추가정보 : " + aditInfo;
                    }else{
                        출력정보 = "결제처리결과 : " + setleSuccesAt + "\n" +
                                "결제메세지 : " + setleMssage + "\n";

                    }
                } else if (cardCashSe.equals("CASH")) {
                    if (setleSuccesAt.equals("O")) {
                        confmNo = uri.getQueryParameter("confmNo");
                        confmDe = uri.getQueryParameter("confmDe");
                        confmTime = uri.getQueryParameter("confmTime");
                        cardNo = uri.getQueryParameter("cardNo");
                        instlmtMonth = uri.getQueryParameter("instlmtMonth");
                        aditInfo = URLDecoder.decode(uri.getQueryParameter("aditInfo"), "UTF-8");

                        출력정보 = "결제처리결과 : " + setleSuccesAt + "\n" +
                                "결제메세지 : " + setleMssage + "\n" +
                                "추가정보 : " + aditInfo;
                    }else{
                        출력정보 = "결제처리결과 : " + setleSuccesAt + "\n" +
                                "결제메세지 : " + setleMssage + "\n";

                    }
                }

                TextView tv = (TextView) this.getView().findViewById(R.id.결과);
                tv.setText(출력정보);

            } catch (Exception e) {
                e.printStackTrace();
                Log.e("fragment",e.toString());
            }
        }
    }
}

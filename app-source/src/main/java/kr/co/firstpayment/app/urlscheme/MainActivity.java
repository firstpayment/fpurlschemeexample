package kr.co.firstpayment.app.urlscheme;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;

import layout.BlankFragment;

public class MainActivity extends AppCompatActivity implements BlankFragment.OnFragmentInteractionListener {

    private String schemejtnet="fpispjtnet://default";
    private String schemeksnet="fpispksnet://default";
    private String schemekis="fpispkis://default";
    private String schemekoces="fpispkoces://default";
    private String schemekovan="fpispkovan://default";
    private String schemekcp="fpispkcp://default";

    private String schemejtnetForReceipt="fpispjtnet://receipt";
    private String schemeksnetForReceipt="fpispksnet://receipt";
    private String schemekisForReceipt="fpispkis://receipt";
    private String schemekocesForReceipt="fpispkoces://receipt";
    private String schemekovanForReceipt="fpispkovan://receipt";
    private String schemekcpForReceipt="fpispkcp://receipt";

    private String schemejtnetForSearch="fpispjtnet://search";
    private String schemeksnetForSearch="fpispksnet://search";
    private String schemekisForSearch="fpispkis://search";
    private String schemekocesForSearch="fpispkoces://search";
    private String schemekovanForSearch="fpispkovan://search";
    private String schemekcpForSearch="fpispkcp://search";

    private String scheme;
    private String schemeForReceipt;
    private String schemeForSearch;

    private String bizrno;
    private String trmnlNo;
    private String splpc;
    private String vat;
    private String taxxpt;
    private String srcConfmNo;
    private String srcConfmDe;
    private String srcInstlmtMonth;

    private String mrhstInfo = "";

    private boolean isNoneSign;
    private boolean isBluetooth;
    private boolean isSendMMSImage;
    private boolean isModifyPrice;
    private boolean isBleConnection;

    CompoundButton mSelectedVan;

    private Handler handler = new Handler();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ((CheckBox)findViewById(R.id.chk_noneSign)).setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                isNoneSign = isChecked;
            }
        });

        ((CheckBox)findViewById(R.id.chk_blutooth)).setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                isBluetooth = isChecked;
                if(isChecked) ((CheckBox)findViewById(R.id.chk_ble_connection)).setVisibility(View.VISIBLE);
                else ((CheckBox)findViewById(R.id.chk_ble_connection)).setVisibility(View.INVISIBLE);
            }
        });

        ((CheckBox)findViewById(R.id.chk_mms_img)).setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                isSendMMSImage = isChecked;
            }
        });

        ((CheckBox)findViewById(R.id.chk_modify_price)).setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                isModifyPrice = isChecked;
            }
        });

        ((CheckBox)findViewById(R.id.chk_ble_connection)).setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                isBleConnection = isChecked;
            }
        });


        // JTNET
        ((RadioButton)findViewById(R.id.jtnet)).setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked) {
                    ((TextView) findViewById(R.id.bizrno)).setText("1078155843");
                    ((TextView) findViewById(R.id.trmnlNo)).setText("1000099991");
                    findViewById(R.id.txt_mrhstInfo).setVisibility(View.INVISIBLE);
                    findViewById(R.id.srcmrhstInfo).setVisibility(View.INVISIBLE);

                    if (mSelectedVan != null) mSelectedVan.setChecked(false);
                    mSelectedVan = buttonView;

                    scheme = schemejtnet;
                    schemeForReceipt = schemejtnetForReceipt;
                    schemeForSearch = schemejtnetForSearch;
                    sVan = "jtnet";
                }
            }
        });

        // KSNET
        ((RadioButton)findViewById(R.id.ksnet)).setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked) {
                    ((TextView) findViewById(R.id.bizrno)).setText("1208197322");
                    ((TextView) findViewById(R.id.trmnlNo)).setText("DPT0TEST03");
                    ((TextView) findViewById(R.id.splpc)).setText("100");
                    ((TextView) findViewById(R.id.vat)).setText("10");
                    findViewById(R.id.txt_mrhstInfo).setVisibility(View.VISIBLE);
                    findViewById(R.id.srcmrhstInfo).setVisibility(View.VISIBLE);
                    findViewById(R.id.layout_info_delngNo).setVisibility(View.VISIBLE);

                    ((TextView) findViewById(R.id.txt_mrhstInfo)).setText("가맹점정보");
                    ((TextView) findViewById(R.id.srcmrhstInfo)).setHint("KSNET전용");

                    if (mSelectedVan != null) mSelectedVan.setChecked(false);
                    mSelectedVan = buttonView;

                    scheme = schemeksnet;
                    schemeForReceipt = schemeksnetForReceipt;
                    schemeForSearch = schemeksnetForSearch;
                    sVan = "ksnet";
                }
            }
        });

        // KIS
        ((RadioButton)findViewById(R.id.kis)).setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked) {
                    ((TextView) findViewById(R.id.bizrno)).setText("1168143939");
                    ((TextView) findViewById(R.id.trmnlNo)).setText("90100546");
                    ((TextView) findViewById(R.id.splpc)).setText("1000");
                    ((TextView) findViewById(R.id.vat)).setText("4");

                    findViewById(R.id.txt_mrhstInfo).setVisibility(View.VISIBLE);
                    findViewById(R.id.srcmrhstInfo).setVisibility(View.VISIBLE);
                    ((TextView) findViewById(R.id.txt_mrhstInfo)).setText("거래번호");
                    ((TextView) findViewById(R.id.srcmrhstInfo)).setHint("KIS전용");

                    if (mSelectedVan != null) mSelectedVan.setChecked(false);
                    mSelectedVan = buttonView;

                    scheme = schemekis;
                    schemeForReceipt = schemekisForReceipt;
                    schemeForSearch = schemekisForSearch;
                    sVan = "kis";
                }
            }
        });

        // KOCES
        ((RadioButton)findViewById(R.id.koces)).setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked) {
                    ((TextView) findViewById(R.id.bizrno)).setText("2148631917");
                    ((TextView) findViewById(R.id.trmnlNo)).setText("0710000900");
                    ((TextView) findViewById(R.id.splpc)).setText("1000");
                    ((TextView) findViewById(R.id.vat)).setText("4");
                    //findViewById(R.id.txt_mrhstInfo).setVisibility(View.INVISIBLE);
                    //findViewById(R.id.srcmrhstInfo).setVisibility(View.INVISIBLE);

                    if (mSelectedVan != null) mSelectedVan.setChecked(false);
                    mSelectedVan = buttonView;

                    scheme = schemekoces;
                    schemeForReceipt = schemekocesForReceipt;
                    schemeForSearch = schemekocesForSearch;
                    sVan = "koces";
                }
            }
        });

        // KOVAN
        ((RadioButton)findViewById(R.id.kovan)).setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked) {
                    ((TextView) findViewById(R.id.bizrno)).setText("2148189243");
                    ((TextView) findViewById(R.id.trmnlNo)).setText("1450019998");
                    ((TextView) findViewById(R.id.splpc)).setText("100");
                    ((TextView) findViewById(R.id.vat)).setText("10");
                    findViewById(R.id.txt_mrhstInfo).setVisibility(View.INVISIBLE);
                    findViewById(R.id.srcmrhstInfo).setVisibility(View.INVISIBLE);

                    if (mSelectedVan != null) mSelectedVan.setChecked(false);
                    mSelectedVan = buttonView;


                    scheme = schemekovan;
                    schemeForReceipt = schemekovanForReceipt;
                    schemeForSearch = schemekovanForSearch;
                    sVan = "kovan";
                }
            }
        });

        // KCP
        ((RadioButton)findViewById(R.id.kcp)).setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked) {
                    ((TextView) findViewById(R.id.bizrno)).setText("1138521083");
                    ((TextView) findViewById(R.id.trmnlNo)).setText("1002189855");
                    ((TextView) findViewById(R.id.splpc)).setText("100");
                    ((TextView) findViewById(R.id.vat)).setText("10");
                    findViewById(R.id.txt_mrhstInfo).setVisibility(View.INVISIBLE);
                    findViewById(R.id.srcmrhstInfo).setVisibility(View.INVISIBLE);

                    if (mSelectedVan != null) mSelectedVan.setChecked(false);
                    mSelectedVan = buttonView;

                    scheme = schemekcp;
                    schemeForReceipt = schemekcpForReceipt;
                    schemeForSearch = schemekcpForSearch;
                    sVan = "kcp";
                }
            }
        });

    }
    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    public void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        Log.d("kmj", "onNewIntent");
        //URLScheme방식으로 호출된경우 결과정보를 얻어서 처리..
        if (Intent.ACTION_VIEW.equals(intent.getAction())) {
            Uri uri = intent.getData();

            try {

                String cardCashSe = uri.getQueryParameter("cardCashSe");

                if(cardCashSe.equals("POINT")) {
                    ((TextView) findViewById(R.id.srcmrhstInfo)).setText(uri.getQueryParameter("cardNo"));
                    Toast.makeText(this, URLDecoder.decode(uri.getQueryParameter("aditInfo"), "UTF-8"), Toast.LENGTH_LONG).show();
                    return;
                }

                String delngSe = uri.getQueryParameter("delngSe");
                String setleSuccesAt = uri.getQueryParameter("setleSuccesAt");
                String setleMssage= URLDecoder.decode(uri.getQueryParameter("setleMssage"), "UTF-8");

                String confmNo = "";
                String confmDe = "";
                String confmTime = "";
                String cardNo = "";
                String instlmtMonth = "";
                String issuCmpnyCode;
                String issuCmpnyNm = "";
                String puchasCmpnyCode;
                String puchasCmpnyNm = "";


                if(cardCashSe.equals("CARD") || cardCashSe.equals("UNION")){
                    if(setleSuccesAt.equals("O")){
                        confmNo = uri.getQueryParameter("confmNo");
                        confmDe = uri.getQueryParameter("confmDe");
                        confmTime = uri.getQueryParameter("confmTime");
                        cardNo = uri.getQueryParameter("cardNo");
                        instlmtMonth = uri.getQueryParameter("instlmtMonth");
                        issuCmpnyCode = uri.getQueryParameter("issuCmpnyCode");
                        issuCmpnyNm = URLDecoder.decode(uri.getQueryParameter("issuCmpnyNm"), "UTF-8");
                        puchasCmpnyCode = uri.getQueryParameter("puchasCmpnyCode");
                        puchasCmpnyNm = URLDecoder.decode(uri.getQueryParameter("puchasCmpnyNm"), "UTF-8");
                    }

                    // 아래 3개는 승인시에만 적용 됩니다.
                    if(delngSe.equals("1")) {
                        splpc = uri.getQueryParameter("splpc");
                        vat = uri.getQueryParameter("vat");
                        taxxpt = uri.getQueryParameter("taxxpt");
                    }

                }else if(cardCashSe.equals("CASH")){
                    if(setleSuccesAt.equals("O")){
                        confmNo = uri.getQueryParameter("confmNo");
                        confmDe = uri.getQueryParameter("confmDe");
                        confmTime = uri.getQueryParameter("confmTime");
                        cardNo = uri.getQueryParameter("cardNo");
                        instlmtMonth = uri.getQueryParameter("instlmtMonth");
                        if(uri.getQueryParameter("delngSe").equals("0"))  // cancel.
                            instlmtMonth += srcInstlmtMonth;

                    }

                    // 아래 3개는 승인시에만 적용 됩니다.
                    if(delngSe.equals("1")) {
                        splpc = uri.getQueryParameter("splpc");
                        vat = uri.getQueryParameter("vat");
                        taxxpt = uri.getQueryParameter("taxxpt");
                    }
                }

                if(sVan.equals("ksnet") && delngSe.equals("1")) {
                    ((EditText)findViewById(R.id.srcdelngNo)).setText(uri.getQueryParameter("delngNo"));
                }
                if(sVan.equals("ksnet") && delngSe.equals("0")
                        && !TextUtils.isEmpty( ((EditText)findViewById(R.id.srcdelngNo)).getText().toString())) {
                    // 이경우 cardNo 가 오지 않습니다.
                    cardNo = "0000";
                }
                String aditInfo = URLDecoder.decode(uri.getQueryParameter("aditInfo"), "UTF-8");

                //String 출력정보="카드정보 : " + cardNo + "결제처리결과 : "+setleSuccesAt+" 결제메세지 : "+setleMssage+" 추가정보: "+aditInfo;
                String 출력정보="카드정보 : " + cardNo + "결제처리결과 : "+setleSuccesAt+" 결제메세지 : "+setleMssage;

                if(sVan.equals("koces")) {
                    출력정보 += " 거래고유키 : " + uri.getQueryParameter("trdKey") + " 가맹점정보 : " + uri.getQueryParameter("mrhstInfo");
                }
                Toast.makeText(this, 출력정보, Toast.LENGTH_LONG).show();
                Log.d("kmj", 출력정보);
                ////////////////////////////////////////////////////////////////
                //만일 프래그먼트에서 데이터를 받아야 한다면 아래의 방식을 참고하여 구현하면 된다.
                ////////////////////////////////////////////////////////////////
                /*BlankFragment fragment = (BlankFragment)this.getSupportFragmentManager().findFragmentById(R.id.프래그먼트);
                fragment.onNewIntent(intent);*/

                try {
                    String rciptNo="0123456789";
                    String mrhst=URLEncoder.encode("홍길동상회","UTF-8");
                    String mrhstAdres=URLEncoder.encode("서울시 강남구 도곡동","UTF-8");
                    String mrhstRprsntv=URLEncoder.encode("홍길동","UTF-8");
                    String mrhstTelno="02-1234-5678";
                    String cstmrTelno="010-1234-5678";

                    if(uri.getQueryParameter("delngSe").equals("0")) {
                        splpc = "-" + splpc;
                        vat = "-" + vat;
                        taxxpt = "-" + taxxpt;
                    }

                    if(TextUtils.isEmpty(splpc)) splpc=((TextView)this.findViewById(R.id.splpc)).getText().toString();
                    if(TextUtils.isEmpty(vat)) vat=((TextView)this.findViewById(R.id.vat)).getText().toString();
                    if(TextUtils.isEmpty(taxxpt)) taxxpt=((TextView)this.findViewById(R.id.taxxpt)).getText().toString();

                    String sendMMSImage = isSendMMSImage?"&mmsSndMth=IMAGE":"";

                    if(TextUtils.isEmpty(issuCmpnyNm)) {
                        issuCmpnyNm = "발급사명없음";
                    }
                    if(TextUtils.isEmpty(puchasCmpnyNm)) {
                        puchasCmpnyNm = "매입사명없음";
                    }
                    final String url = schemeForReceipt+"?"+
                            "bizrno="+bizrno+
                            "&trmnlNo="+trmnlNo+
                            "&mrhst="+mrhst+
                            "&mrhstAdres="+mrhstAdres+
                            "&mrhstRprsntv="+mrhstRprsntv+
                            "&mrhstTelno="+mrhstTelno+
                            "&cardCashSe="+cardCashSe+
                            "&delngSe="+delngSe+
                            "&setleMssage="+setleMssage+
                            "&rciptNo="+rciptNo+
                            "&confmNo="+confmNo+
                            "&confmDe="+confmDe+
                            "&confmTime="+confmTime+
                            "&cardNo="+cardNo+
                            "&instlmtMonth="+instlmtMonth+
                            "&issuCmpnyNm="+issuCmpnyNm+
                            "&puchasCmpnyNm="+puchasCmpnyNm+
                            "&splpc="+splpc+
                            "&vat="+vat+
                            "&taxxpt="+taxxpt+
                            "&cstmrTelno="+cstmrTelno + sendMMSImage;

                    Log.e("TEST", url);
                    handler.postDelayed(new Runnable() {
                        public void run() {
                            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                            startActivity(intent);
                        }
                    }, 1000);


                }catch(UnsupportedEncodingException e){
                    e.printStackTrace();
                }catch(ActivityNotFoundException e) {
                    e.printStackTrace();
                    //해당하는 앱이없어 앱호출에 실패한 것이므로 마켓에서 앱을 다운로드 받도록 유도해야한다.
                    Intent marketLaunch = new Intent(Intent.ACTION_VIEW);
                    if(sVan.equals("jtnet")){
                        marketLaunch.setData(Uri.parse("market://details?id=kr.co.firstpayment.app.jtnet"));
                    }else if(sVan.equals("ksnet")){
                        marketLaunch.setData(Uri.parse("market://details?id=kr.co.firstpayment.app.ksnet"));
                    }else if(sVan.equals("kis")){
                        marketLaunch.setData(Uri.parse("market://details?id=kr.co.firstpayment.app.kis"));
                    }else if(sVan.equals("koces")){
                        marketLaunch.setData(Uri.parse("market://details?id=kr.co.firstpayment.app.koces"));
                    }else if(sVan.equals("kovan")){
                        marketLaunch.setData(Uri.parse("market://details?id=kr.co.firstpayment.app.kovan"));
                    }else if(sVan.equals("kcp")){
                        marketLaunch.setData(Uri.parse("market://details?id=kr.co.firstpayment.app.kcp"));
                    }
                    this.startActivity(marketLaunch);
                }


            }catch(Exception e) {
                Toast.makeText(this, e.toString(), Toast.LENGTH_SHORT).show();
            }


        }
    }

    @Override
    protected void onResume() {
        super.onResume();


    }

    private String sVan;
    private void 밴체크(){

        bizrno=((TextView)this.findViewById(R.id.bizrno)).getText().toString();
        trmnlNo=((TextView)this.findViewById(R.id.trmnlNo)).getText().toString();
        splpc=((TextView)this.findViewById(R.id.splpc)).getText().toString();
        vat=((TextView)this.findViewById(R.id.vat)).getText().toString();
        taxxpt=((TextView)this.findViewById(R.id.taxxpt)).getText().toString();
        srcConfmNo=((TextView)this.findViewById(R.id.srcConfmNo)).getText().toString();
        srcConfmDe=((TextView)this.findViewById(R.id.srcConfmDe)).getText().toString();
        srcInstlmtMonth=((TextView)this.findViewById(R.id.srcInstlmtMonth)).getText().toString();


    }

    public void TEST(View view) {
        if(true) return;
        try {
            String callbackAppUrl = "callbackapp://default";
            String aditInfo = URLEncoder.encode("동해물과= 백두산이&마르=   고닳도록&하느님이=보우하사 우리나라만세", "UTF-8");
            cardCashSe = "POINT";
            String trmnlCnnc = "";
            if (isBluetooth) trmnlCnnc = "&trmnlCnnc=blth";

            String url = scheme + "?" +
                    "&cardCashSe=" + cardCashSe + trmnlCnnc+
                    "&callbackAppUrl="+callbackAppUrl+
                    "&aditInfo="+aditInfo;
            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
            startActivity(intent);
        }catch(UnsupportedEncodingException e){

        }catch(ActivityNotFoundException e) {
            //해당하는 앱이없어 앱호출에 실패한 것이므로 마켓에서 앱을 다운로드 받도록 유도해야한다.
            Intent marketLaunch = new Intent(Intent.ACTION_VIEW);
            if(sVan.equals("jtnet")){
                marketLaunch.setData(Uri.parse("market://details?id=kr.co.firstpayment.app.jtnet"));
            }else if(sVan.equals("ksnet")){
                marketLaunch.setData(Uri.parse("market://details?id=kr.co.firstpayment.app.ksnet"));
            }else if(sVan.equals("kis")){
                marketLaunch.setData(Uri.parse("market://details?id=kr.co.firstpayment.app.kis"));
            }else if(sVan.equals("koces")){
                marketLaunch.setData(Uri.parse("market://details?id=kr.co.firstpayment.app.koces"));
            }else if(sVan.equals("kovan")){
                marketLaunch.setData(Uri.parse("market://details?id=kr.co.firstpayment.app.kovan"));
            }else if(sVan.equals("kcp")){
                marketLaunch.setData(Uri.parse("market://details?id=kr.co.firstpayment.app.kcp"));
            }
            this.startActivity(marketLaunch);
        }
    }

    String cardCashSe = "CARD";
    public void 은련카드결제정보로앱호출(View view){
        cardCashSe = "UNION";
        카드결제정보로앱호출();
    }

    public void 카드결제정보로앱호출(View view){
        cardCashSe = "CARD";
        카드결제정보로앱호출();
    }
    public void 카드결제정보로앱호출(){

        밴체크();

        try {

            String delngSe="1";
            String callbackAppUrl="callbackapp://default";
            String aditInfo= URLEncoder.encode("동해물과= 백두산이&마르=   고닳도록&하느님이=보우하사 우리나라만세","UTF-8");
            if(scheme.equals(schemeksnet)) { // 세스코만 해당. 아래 주석풀고 사용하면됨.
                //aditInfo = "nosigndata" + aditInfo;
            }

            if(isNoneSign)
                srcInstlmtMonth += "&noneSign=X";

            if(sVan.equals("ksnet") || sVan.equals("koces")) {
                String srcmrhstInfo=((TextView)this.findViewById(R.id.srcmrhstInfo)).getText().toString();
                //mrhstInfo = "&mrhstInfo=" + URLEncoder.encode(srcmrhstInfo, "UTF-8") + "&isPG=O";
                mrhstInfo = "&mrhstInfo=" + URLEncoder.encode(srcmrhstInfo, "UTF-8");
            }
            else if(sVan.equals("kis")) {
                String delngNo =((TextView)this.findViewById(R.id.srcmrhstInfo)).getText().toString();
                mrhstInfo = "&delngNo=" + URLEncoder.encode(delngNo, "UTF-8");
            }

            String trmnlCnnc = "";
            if(isBluetooth) trmnlCnnc = "&trmnlCnnc=blth";

            String setDlamtApdt = "";
            if(isModifyPrice) setDlamtApdt ="&setDlamtApdt=O";

            String url = scheme+"?"+
                         "bizrno="+bizrno+
                         "&trmnlNo="+trmnlNo+
                         "&cardCashSe="+cardCashSe+
                         "&delngSe="+delngSe+
                         "&splpc="+splpc+
                         "&vat="+vat+
                         "&taxxpt="+taxxpt+
                         "&callbackAppUrl="+callbackAppUrl+
                         "&aditInfo="+aditInfo+
                         "&srcConfmNo="+srcConfmNo+
                         "&srcConfmDe="+srcConfmDe+
                         "&srcInstlmtMonth="+srcInstlmtMonth+
                        mrhstInfo + trmnlCnnc + setDlamtApdt;

            if(isBleConnection) url += "&bleConnection=O";

            //url = "fpispjtnet://default?bizrno=117-12-25262&trmnlNo=28805734&cardCashSe=CARD&delngSe=1&splpc=18&vat=2&taxxpt=0&callbackAppUrl=callback_Card://default&aditInfo=fpispjtnet";
            //url = "fpispksnet://default?bizrno=1028197322&trmnlNo=DPT0A07607&cardCashSe=CARD&delngSe=1&splpc=909&vat=91&taxxpt=0&callbackAppUrl=gymstaff://default&aditInfo=842&srcConfmNo=&srcConfmDe=&srcInstlmtMonth=&cstmrTelno=&noneSign=&mrhstInfo=";
            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
            startActivity(intent);

        }catch(UnsupportedEncodingException e){

        }catch(ActivityNotFoundException e) {
            //해당하는 앱이없어 앱호출에 실패한 것이므로 마켓에서 앱을 다운로드 받도록 유도해야한다.
            Intent marketLaunch = new Intent(Intent.ACTION_VIEW);
            if(sVan.equals("jtnet")){
                marketLaunch.setData(Uri.parse("market://details?id=kr.co.firstpayment.app.jtnet"));
            }else if(sVan.equals("ksnet")){
                marketLaunch.setData(Uri.parse("market://details?id=kr.co.firstpayment.app.ksnet"));
            }else if(sVan.equals("kis")){
                marketLaunch.setData(Uri.parse("market://details?id=kr.co.firstpayment.app.kis"));
            }else if(sVan.equals("koces")){
                marketLaunch.setData(Uri.parse("market://details?id=kr.co.firstpayment.app.koces"));
            }else if(sVan.equals("kovan")){
                marketLaunch.setData(Uri.parse("market://details?id=kr.co.firstpayment.app.kovan"));
            }else if(sVan.equals("kcp")){
                marketLaunch.setData(Uri.parse("market://details?id=kr.co.firstpayment.app.kcp"));
            }
            this.startActivity(marketLaunch);
        }
    }

    public void 은련카드취소정보로앱호출(View view){
        cardCashSe = "UNION";
        카드취소정보로앱호출();
    }
    public void 카드취소정보로앱호출(View view){
        cardCashSe = "CARD";
        카드취소정보로앱호출();
    }
    public void 카드취소정보로앱호출(){

        밴체크();

        try {

            String delngSe="0";
            String callbackAppUrl="callbackapp://default";
            String aditInfo=URLEncoder.encode("동해물과= 백두산이&마르=   고닳도록&하느님이=보우하사 우리나라만세","UTF-8");
            if(scheme.equals(schemeksnet)) { // 세스코만 해당. 아래 주석풀고 사용하면됨.
                //aditInfo = "nosigndata" + aditInfo;
            }

            if(isNoneSign)
                srcInstlmtMonth += "&noneSign=X";
            else srcInstlmtMonth += "&noneSign=O";

            if(sVan.equals("ksnet") || sVan.equals("koces"));
                //mrhstInfo = "&mrhstInfo=" + URLEncoder.encode("test1234=가맹점정보", "UTF-8");

            else if(sVan.equals("kis")) {
                String delngNo =((TextView)this.findViewById(R.id.srcmrhstInfo)).getText().toString();
                mrhstInfo = "&delngNo=" + URLEncoder.encode(delngNo, "UTF-8");
            }

            // 2018.05.15 KMJ KSNet 전용 기능 추가.
            // 거래고유번호 ( 옵션이 존재하면 취소시 카드 읽지 않음 )
            if(sVan.equals("ksnet")) {
                String delngNo =((TextView)this.findViewById(R.id.srcdelngNo)).getText().toString();
                if(!TextUtils.isEmpty(delngNo))
                    mrhstInfo += "&delngNo=" + URLEncoder.encode(delngNo, "UTF-8");
            }

            String trmnlCnnc = "";
            if(isBluetooth) trmnlCnnc = "&trmnlCnnc=blth";

            String url = scheme+"?"+
                    "bizrno="+bizrno+
                    "&trmnlNo="+trmnlNo+
                    "&cardCashSe="+cardCashSe+
                    "&delngSe="+delngSe+
                    "&splpc="+splpc+
                    "&vat="+vat+
                    "&taxxpt="+taxxpt+
                    "&callbackAppUrl="+callbackAppUrl+
                    "&aditInfo="+aditInfo+
                    "&srcConfmNo="+srcConfmNo+
                    "&srcConfmDe="+srcConfmDe+
                    "&srcInstlmtMonth="+srcInstlmtMonth+
                    mrhstInfo + trmnlCnnc;

            if(isBleConnection) url += "&bleConnection=O";

            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
            startActivity(intent);

        }catch(UnsupportedEncodingException e){

        }catch(ActivityNotFoundException e) {
            //해당하는 앱이없어 앱호출에 실패한 것이므로 마켓에서 앱을 다운로드 받도록 유도해야한다.
            Intent marketLaunch = new Intent(Intent.ACTION_VIEW);
            if(sVan.equals("jtnet")){
                marketLaunch.setData(Uri.parse("market://details?id=kr.co.firstpayment.app.jtnet"));
            }else if(sVan.equals("ksnet")){
                marketLaunch.setData(Uri.parse("market://details?id=kr.co.firstpayment.app.ksnet"));
            }else if(sVan.equals("kis")){
                marketLaunch.setData(Uri.parse("market://details?id=kr.co.firstpayment.app.kis"));
            }else if(sVan.equals("koces")){
                marketLaunch.setData(Uri.parse("market://details?id=kr.co.firstpayment.app.koces"));
            }else if(sVan.equals("kovan")){
                marketLaunch.setData(Uri.parse("market://details?id=kr.co.firstpayment.app.kovan"));
            }else if(sVan.equals("kcp")){
                marketLaunch.setData(Uri.parse("market://details?id=kr.co.firstpayment.app.kcp"));
            }
            this.startActivity(marketLaunch);
        }
    }
    public void 현금결제정보로앱호출(View view){

        밴체크();

        try {

            String cardCashSe="CASH";
            String delngSe="1";
            String callbackAppUrl="callbackapp://default";
            String aditInfo=URLEncoder.encode("동해물과= 백두산이&마르=   고닳도록&하느님이=보우하사 우리나라만세","UTF-8");
            String cstmrTelno="010-123-4567";

            if(sVan.equals("ksnet") || sVan.equals("koces"));
                //mrhstInfo = "&mrhstInfo=" + URLEncoder.encode("test1234=가맹점정보", "UTF-8");

            else if(sVan.equals("kis")) {
                String delngNo =((TextView)this.findViewById(R.id.srcmrhstInfo)).getText().toString();
                mrhstInfo = "&delngNo=" + URLEncoder.encode(delngNo, "UTF-8");
            }

            String trmnlCnnc = "";
            if(isBluetooth) trmnlCnnc = "&trmnlCnnc=blth";

            String setDlamtApdt = "";
            if(isModifyPrice) setDlamtApdt ="&setDlamtApdt=O";

            String url = scheme+"?"+
                    "bizrno="+bizrno+
                    "&trmnlNo="+trmnlNo+
                    "&cardCashSe="+cardCashSe+
                    "&delngSe="+delngSe+
                    "&splpc="+splpc+
                    "&vat="+vat+
                    "&taxxpt="+taxxpt+
                    "&callbackAppUrl="+callbackAppUrl+
                    "&aditInfo="+aditInfo+
                    "&srcConfmNo="+srcConfmNo+
                    "&srcConfmDe="+srcConfmDe+
                    "&srcInstlmtMonth="+srcInstlmtMonth+
                    "&cstmrTelno="+cstmrTelno + mrhstInfo + trmnlCnnc + setDlamtApdt;


            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
            startActivity(intent);
            //this.finish();
        }catch(UnsupportedEncodingException e){

        }catch(ActivityNotFoundException e) {
            //해당하는 앱이없어 앱호출에 실패한 것이므로 마켓에서 앱을 다운로드 받도록 유도해야한다.
            Intent marketLaunch = new Intent(Intent.ACTION_VIEW);
            if(sVan.equals("jtnet")){
                marketLaunch.setData(Uri.parse("market://details?id=kr.co.firstpayment.app.jtnet"));
            }else if(sVan.equals("ksnet")){
                marketLaunch.setData(Uri.parse("market://details?id=kr.co.firstpayment.app.ksnet"));
            }else if(sVan.equals("kis")){
                marketLaunch.setData(Uri.parse("market://details?id=kr.co.firstpayment.app.kis"));
            }else if(sVan.equals("koces")){
                marketLaunch.setData(Uri.parse("market://details?id=kr.co.firstpayment.app.koces"));
            }else if(sVan.equals("kovan")){
                marketLaunch.setData(Uri.parse("market://details?id=kr.co.firstpayment.app.kovan"));
            }else if(sVan.equals("kcp")){
                marketLaunch.setData(Uri.parse("market://details?id=kr.co.firstpayment.app.kcp"));
            }
            this.startActivity(marketLaunch);
        }
    }
    public void 현금취소정보로앱호출(View view){

        밴체크();

        try {

            String cardCashSe="CASH";
            String delngSe="0";
            String callbackAppUrl="callbackapp://default";
            String aditInfo=URLEncoder.encode("동해물과= 백두산이&마르=   고닳도록&하느님이=보우하사 우리나라만세","UTF-8");
            String cstmrTelno="010-123-4567";

            if(sVan.equals("ksnet") || sVan.equals("koces"));
                //mrhstInfo = "&mrhstInfo=" + URLEncoder.encode("test1234=가맹점정보", "UTF-8");

            else if(sVan.equals("kis")) {
                String delngNo =((TextView)this.findViewById(R.id.srcmrhstInfo)).getText().toString();
                mrhstInfo = "&delngNo=" + URLEncoder.encode(delngNo, "UTF-8");
            }

            String trmnlCnnc = "";
            if(isBluetooth) trmnlCnnc = "&trmnlCnnc=blth";

            String url = scheme+"?"+
                    "bizrno="+bizrno+
                    "&trmnlNo="+trmnlNo+
                    "&cardCashSe="+cardCashSe+
                    "&delngSe="+delngSe+
                    "&splpc="+splpc+
                    "&vat="+vat+
                    "&taxxpt="+taxxpt+
                    "&callbackAppUrl="+callbackAppUrl+
                    "&aditInfo="+aditInfo+
                    "&srcConfmNo="+srcConfmNo+
                    "&srcConfmDe="+srcConfmDe+
                    "&srcInstlmtMonth="+srcInstlmtMonth+
                    "&cstmrTelno="+cstmrTelno + mrhstInfo + trmnlCnnc;

            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
            startActivity(intent);

        }catch(UnsupportedEncodingException e){

        }catch(ActivityNotFoundException e) {
            //해당하는 앱이없어 앱호출에 실패한 것이므로 마켓에서 앱을 다운로드 받도록 유도해야한다.
            Intent marketLaunch = new Intent(Intent.ACTION_VIEW);
            if(sVan.equals("jtnet")){
                marketLaunch.setData(Uri.parse("market://details?id=kr.co.firstpayment.app.jtnet"));
            }else if(sVan.equals("ksnet")){
                marketLaunch.setData(Uri.parse("market://details?id=kr.co.firstpayment.app.ksnet"));
            }else if(sVan.equals("kis")){
                marketLaunch.setData(Uri.parse("market://details?id=kr.co.firstpayment.app.kis"));
            }else if(sVan.equals("koces")){
                marketLaunch.setData(Uri.parse("market://details?id=kr.co.firstpayment.app.koces"));
            }else if(sVan.equals("kovan")){
                marketLaunch.setData(Uri.parse("market://details?id=kr.co.firstpayment.app.kovan"));
            }else if(sVan.equals("kcp")){
                marketLaunch.setData(Uri.parse("market://details?id=kr.co.firstpayment.app.kcp"));
            }
            this.startActivity(marketLaunch);
        }
    }


    public void 영수증호출(View view){

        밴체크();

        try {
            String rciptNo="201603300001";
            String mrhst=URLEncoder.encode("홍길동상회","UTF-8");
            String mrhstAdres=URLEncoder.encode("서울시 강남구 도곡동","UTF-8");
            String mrhstRprsntv=URLEncoder.encode("홍길동","UTF-8");
            String mrhstTelno="02-1234-5678";
            String cardCashSe="CARD";
            String delngSe="1";
            String setleMssage= URLEncoder.encode("잔액 : 0000","UTF-8");
            String confmTime="122655";
            String cardNo="1234-56XX-XXXX-XX32"; //예시임..
            String issuCmpnyNm=URLEncoder.encode("발급카드사명","UTF-8");
            String puchasCmpnyNm=URLEncoder.encode("매입카드사명","UTF-8");
            String cstmrTelno="010-1234-5678";

            String url = schemeForReceipt+"?"+
                    "bizrno="+bizrno+
                    "&trmnlNo="+trmnlNo+
                    "&mrhst="+mrhst+
                    "&mrhstAdres="+mrhstAdres+
                    "&mrhstRprsntv="+mrhstRprsntv+
                    "&mrhstTelno="+mrhstTelno+
                    "&cardCashSe="+cardCashSe+
                    "&delngSe="+delngSe+
                    "&setleMssage="+setleMssage+
                    "&rciptNo="+rciptNo+
                    "&confmNo="+srcConfmNo+
                    "&confmDe="+srcConfmDe+
                    "&confmTime="+confmTime+
                    "&cardNo="+cardNo+
                    "&instlmtMonth="+srcInstlmtMonth+
                    "&issuCmpnyNm="+issuCmpnyNm+
                    "&puchasCmpnyNm="+puchasCmpnyNm+
                    "&splpc="+splpc+
                    "&vat="+vat+
                    "&taxxpt="+taxxpt+
                    "&cstmrTelno="+cstmrTelno;

            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
            startActivity(intent);

        }catch(UnsupportedEncodingException e){
        }catch(ActivityNotFoundException e) {
            //해당하는 앱이없어 앱호출에 실패한 것이므로 마켓에서 앱을 다운로드 받도록 유도해야한다.
            Intent marketLaunch = new Intent(Intent.ACTION_VIEW);
            if(sVan.equals("jtnet")){
                marketLaunch.setData(Uri.parse("market://details?id=kr.co.firstpayment.app.jtnet"));
            }else if(sVan.equals("ksnet")){
                marketLaunch.setData(Uri.parse("market://details?id=kr.co.firstpayment.app.ksnet"));
            }else if(sVan.equals("kis")){
                marketLaunch.setData(Uri.parse("market://details?id=kr.co.firstpayment.app.kis"));
            }else if(sVan.equals("koces")){
                marketLaunch.setData(Uri.parse("market://details?id=kr.co.firstpayment.app.koces"));
            }else if(sVan.equals("kovan")){
                marketLaunch.setData(Uri.parse("market://details?id=kr.co.firstpayment.app.kovan"));
            }else if(sVan.equals("kcp")){
                marketLaunch.setData(Uri.parse("market://details?id=kr.co.firstpayment.app.kcp"));
            }
            this.startActivity(marketLaunch);
        }
    }

    public void 조회하기(View view) {
        밴체크();

        try {
            String url = schemeForSearch;
            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
            startActivity(intent);
        }catch(ActivityNotFoundException e) {
            //해당하는 앱이없어 앱호출에 실패한 것이므로 마켓에서 앱을 다운로드 받도록 유도해야한다.
            Intent marketLaunch = new Intent(Intent.ACTION_VIEW);
            if (sVan.equals("jtnet")) {
                marketLaunch.setData(Uri.parse("market://details?id=kr.co.firstpayment.app.jtnet"));
            } else if (sVan.equals("ksnet")) {
                marketLaunch.setData(Uri.parse("market://details?id=kr.co.firstpayment.app.ksnet"));
            } else if (sVan.equals("kis")) {
                marketLaunch.setData(Uri.parse("market://details?id=kr.co.firstpayment.app.kis"));
            } else if (sVan.equals("koces")) {
                marketLaunch.setData(Uri.parse("market://details?id=kr.co.firstpayment.app.koces"));
            } else if (sVan.equals("kovan")) {
                marketLaunch.setData(Uri.parse("market://details?id=kr.co.firstpayment.app.kovan"));
            }else if(sVan.equals("kcp")){
                marketLaunch.setData(Uri.parse("market://details?id=kr.co.firstpayment.app.kcp"));
            }
        }
    }

    @Override
    public void onFragmentInteraction(Uri uri) {
        Log.d("Main","onFragmentInteraction");
    }
}
